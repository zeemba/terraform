const http = require('http');
const os = require('os');

const port = 8080;
const greeting = process.env.GREETING || 'Hello';
const user = process.env.USER || 'World';

const server = http.createServer((req, res) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/plain');
  res.end(`${greeting} from ${user} on ${os.hostname()}!`);
});

server.listen(port, () => {
  console.log(`Server running at http://localhost:${port}/`);
  console.log(`Environment variables: GREETING=${greeting}, USER=${user}`);
});
