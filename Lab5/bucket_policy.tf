locals {
  actions_metadata_bucket = [
    "s3:GetAccelerateConfiguration",
    "s3:GetAnalyticsConfiguration",
    "s3:GetBucketAcl",
    "s3:GetBucketCORS",
    "s3:GetBucketLocation",
    "s3:GetBucketLogging",
    "s3:GetBucketNotification",
    "s3:GetBucketObjectLockConfiguration",
    "s3:GetBucketOwnershipControls",
    "s3:GetBucketPolicy",
    "s3:GetBucketPolicyStatus",
    "s3:GetBucketPublicAccessBlock",
    "s3:GetBucketRequestPayment",
    "s3:GetBucketTagging",
    "s3:GetBucketVersioning",
    "s3:GetBucketWebsite",
    "s3:GetEncryptionConfiguration",
    "s3:GetIntelligentTieringConfiguration",
    "s3:GetInventoryConfiguration",
    "s3:GetLifecycleConfiguration",
    "s3:GetMetricsConfiguration",
    "s3:GetReplicationConfiguration",
    "s3:ListBucket",
    "s3:ListBucketMultipartUploads",
    "s3:ListBucketVersions",
    # "s3:ListJobs",
    "s3:ListMultipartUploadParts",
    # "s3:ListStorageLensConfigurations"
  ]

  actions_metadata_object = [
    "s3:GetObjectAcl",
    "s3:GetObjectLegalHold",
    "s3:GetObjectRetention",
    "s3:GetObjectTagging",
    "s3:GetObjectVersionAcl",
    "s3:GetObjectVersionTagging"
  ]

  actions_tagging = [
    "s3:PutBucketTagging",
    # "s3:PutJobTagging",
    "s3:PutObjectTagging",
    "s3:PutObjectVersionTagging",
    "s3:ReplicateTags"
  ]

  built_ins = [{
    sid              = "AdministrationOfTheBucket"
    # Added app CICD user and current caller identity  as owners of the bucket per CSE-10514
    role             = [data.aws_iam_role.app_cicd.arn, data.aws_iam_user.appcicd.arn, data.aws_caller_identity.current.arn]
    roletype         = "AWS"
    condition_type   = "StringEquals"
    condition_var    = "aws:PrincipalArn"
    condition_values = [data.aws_iam_role.app_cicd.arn]
    allow_actions    = ["s3:*"]
    },
    {
      sid              = "CloudServicesMetadata"
      role             = ["arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/sso-cloud-eng"]
      roletype         = "AWS"
      condition_type   = "StringEquals"
      condition_var    = "s3:signatureversion"
      condition_values = ["AWS4-HMAC-SHA256"]
      allow_actions    = flatten([local.actions_tagging, local.actions_metadata_object, local.actions_metadata_bucket])
    },
    {
      sid              = "StealthAudit"
      role             = ["arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/bokf-4230-gbl-role-stealth-audit"]
      roletype         = "AWS"
      condition_type   = "IpAddress"
      condition_var    = "aws:SourceIPAddress"
      condition_values = ["216.60.18.0/24", "63.97.206.0/24"]
      allow_actions    = ["s3:*"]
  }]

  DenyRoleConditionType = {
    Bool            = "Bool"
    IpAddress       = "NotIpAddress"
    NotIpAddress    = "IpAddress"
    StringEquals    = "StringNotEquals"
    StringNotEquals = "StringEquals"
  }

  temp_full_list = flatten([var.iam-policy-users-map, local.built_ins])
  full_list = flatten([
    for item in local.temp_full_list :
    [for index, i in item.role :
    merge(item, { role = i, sid = join("", [item.sid, index]) })]
  ])
  unique_roles = [for item in local.full_list : item["role"]]
}

data "aws_iam_policy_document" "this" {
  // Statement 1 = Deny Unencrypted

  statement {
    sid    = "DenyUnencrypted"
    effect = "Deny"

    principals {
      type        = "*"
      identifiers = ["*"]
    }

    actions   = ["s3:*"]
    resources = ["arn:aws:s3:::${local.s3_name}/*"]

    condition {
      test     = "Bool"
      variable = "aws:SecureTransport"
      values   = ["false"]
    }
  }

  // Statement 2 = Deny If Not Allowed
  statement {
    sid    = "DenyIfNotAllowed"
    effect = "Deny"
    principals {
      type        = "*"
      identifiers = ["*"]
    }
    actions   = ["s3:*"]
    resources = ["arn:aws:s3:::${local.s3_name}", "arn:aws:s3:::${local.s3_name}/*"]
    dynamic "condition" {
      for_each = length([for item in local.full_list : lookup(item, "roletype") == "AWS" ? [item] : []]) > 0 ? [1] : []
      content {
        test     = "StringNotEquals"
        variable = "aws:PrincipalArn"
        values   = distinct(compact([for item in local.full_list : lookup(item, "roletype") == "AWS" ? item["role"] : ""]))
      }
    }
    dynamic "condition" {
      for_each = length(flatten([for item in local.full_list : lookup(item, "roletype") == "Service" ? [item] : []])) > 0 ? [1] : []
      content {
        test     = "StringNotEquals"
        variable = "aws:PrincipalServiceName"
        values   = distinct(compact([for item in local.full_list : lookup(item, "roletype") == "Service" ? item["role"] : ""]))
      }
    }
  }


  // Statement 3 - Deny Per Role
  dynamic "statement" {
    for_each = flatten([for item in local.full_list : lookup(item, "allow_actions", null) != null && !contains(lookup(item, "allow_actions", null), "s3:*") ? [item] : []])
    content {
      sid    = "DenyRoleIfNot${statement.value["sid"]}"
      effect = "Deny"
      principals {
        type        = statement.value["roletype"]
        identifiers = [statement.value["role"]]
      }
      not_actions = statement.value["allow_actions"]
      resources   = ["arn:aws:s3:::${local.s3_name}", "arn:aws:s3:::${local.s3_name}/*"]
      dynamic "condition" {
        for_each = lookup(statement.value, "condition_var", null) == null && statement.value["roletype"] == "Service" ? [] : [1]
        content {
          #test     = statement.value["condition_type"]
          test     = lookup(local.DenyRoleConditionType, statement.value["condition_type"])
          variable = statement.value["condition_var"]
          values   = statement.value["condition_values"]
        }
      }
    }
  }


  // Statement 4 - Deny Per Role If Not Allowed Action
  dynamic "statement" {
    for_each = flatten([for item in local.full_list : lookup(item, "allow_actions", null) != null && !contains(lookup(item, "allow_actions", null), "s3:*") ? [item] : []])
    content {
      sid    = "DenyActionIfNot${statement.value["sid"]}"
      effect = "Deny"
      principals {
        type        = statement.value["roletype"]
        identifiers = [statement.value["role"]]
      }
      not_actions = statement.value["allow_actions"]
      resources   = ["arn:aws:s3:::${local.s3_name}", "arn:aws:s3:::${local.s3_name}/*"]
    }
  }

  // Statement 5 - Allowed Action
  dynamic "statement" {
    for_each = flatten([for item in local.full_list : lookup(item, "allow_actions", null) != null ? [item] : []])
    content {
      sid    = "AllowAction${statement.value["sid"]}"
      effect = "Allow"
      principals {
        type        = statement.value["roletype"]
        identifiers = [statement.value["role"]]
      }
      actions   = statement.value["allow_actions"]
      resources = ["arn:aws:s3:::${local.s3_name}", "arn:aws:s3:::${local.s3_name}/*"]
    }
  }
}