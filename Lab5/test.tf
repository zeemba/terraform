resource "aws_instance" "this" {
  ami           = length(data.aws_ssm_parameter.ami) > 0 ? data.aws_ssm_parameter.ami[0].value : data.aws_ami.input[0].id
  instance_type = var.node_config.instance_type

  tags = merge(module.standard_tags.tags,
    {
      Name = local.name
    }
  )

  user_data        = var.user_data
  user_data_base64 = var.user_data_base64

  network_interface {
    network_interface_id = aws_network_interface.this.id
    device_index         = 0
  }

  credit_specification {
    cpu_credits = "unlimited"
  }

  root_block_device {
    volume_type = var.node_config.volume_type
    volume_size = var.node_config.volume_size
    tags = merge(module.standard_tags.tags,
      {
        Name = local.ebs_name
      }
    )
    encrypted             = true
    kms_key_id            = var.ebs_key != null ? data.aws_kms_key.ebs_key[0].arn : module.ebs_key[0].kms_arn
    delete_on_termination = true
  }
}