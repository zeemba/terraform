# Using data block to fetch the subnet id based on environment/account
data "aws_subnet" "selected" {
  for_each        = local.subnet_name
  filter {
    name    = "tag:Name"
    values = [each.value]

  }
}
locals {

  json_files = fileset("${path.root}", "*.json")
  # Map instance type provided in SNOW request form to actual AWS instance_type
  instance_type_mapping = {
    "nano"      = "t3.nano"
    "micro"     = "t3.micro"
    "small"     = "t3.small"
    "medium"    = "t3.medium"
    "large"     = "m5.large"
    "xlarge"    = "m5.xlarge"
    "2xlarge"   = "m5.2xlarge"
    "4xlarge"   = "m5.4xlarge"
    "8xlarge"   = "m5.8xlarge"
    "16xlarge"  = "m5.16xlarge"
  }
  #Convert the json file provided into a map that can be ingested by the module
  ec2_instance_configs = {
    for file in fileset("${path.root}", "*.json") :
    # The merge function here merges the first map which loads the entire json file and second map which overrides
    # the initial value of the instance_type to the actual instance_type needed for the module based on mapping lookup
    trimsuffix(file, ".json") => merge(
      jsondecode(file("${path.root}/${file}")),
      {
        instance_type = lookup(local.instance_type_mapping, jsondecode(file("${path.root}/${file}")).instance_type, jsondecode(file("${path.root}/${file}")).instance_type)
      }
    )
  }
  # Set subnet name to be used by the data block
  subnet_name = {
    for key, config in local.target_env_configs :
    key => join("-", ["bokf", config.application_id, config.environment, "us-east-2a", "snet-private"])
  }

  target_env_configs = { for k, v in local.ec2_instance_configs : k => v if v.environment == var.environment || v.environment != "prd" && var.environment != "prd" }
  
}
module "ec2-instance" {
  source          = "app.terraform.io/bokf-beta/ec2-instance/aws"
  version         = "0.4.3"
  for_each        = local.target_env_configs
  tags = {
    environment         = each.value.environment
    application_id      = each.value.application_id
    cost_center         = each.value.cost_center
    asset_class         = each.value.asset_classification
    data_classification = each.value.data_classification
    managed_by          = var.managed_by
    source_code         = var.source_code
    deployed_by         = var.TFC_WORKSPACE_NAME
    requested_by        = each.value.requested_for_email
    application_role    = "web"
  }
  # node_config = each.value
  node_config     =  {
    instance_type          = each.value.instance_type
    volume_size            = each.value.volume_size
    ec2_name               = each.value.machine_name
    node_role_arn          = null
    asset_classification   = each.value.asset_classification
    cost_center            = each.value.cost_center
    application_id         = each.value.application_id
    data_classification    = each.value.data_classification
    requested_by           = each.value.requested_for_email
    security_group_ids     = null
    stage                  = "current"
    subnet_id              = data.aws_subnet.selected[each.key].id
    volume_type            = "gp2"
    tags = {
      environment         = each.value.environment
      application_id      = each.value.application_id
      cost_center         = each.value.cost_center
      asset_class         = each.value.asset_classification
      data_classification = each.value.data_classification
      managed_by          = var.managed_by
      source_code         = var.source_code
      deployed_by         = var.TFC_WORKSPACE_NAME
      requested_by        = each.value.requested_for_email
      application_role    = "web"
    }
  }
  ami_config      = {
        name      = null
        id        = null
        target_os = each.value.target_os
  }
}
