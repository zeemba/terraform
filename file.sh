#!/usr/bin/env bash
#
# DESCRIPTION: 
# This will generate Managed Streaming Kafka
# Certificates based on input values

# DEPENDENCY:
# This script relies on Java Runtime (JRE) and Java Development Kit (JDK)

# EXAMPLE:
# bash aws_msk_cert.sh "bdwdev" "arn:aws:acm-pca:us-east-2:0123456789123:certificate-authority/abcde-12391-123490823-123123412" "test_dsg" ~/msk_certs

# Output all log information to /tmp/startup_log.txt
# exec &> /tmp/aws_msk_cert_script.txt

# These are input values
alias="$1"
pca_arn="$2"
aws_profile="$3"
output="$4"

passphrase="$(openssl rand -base64 16)"
days_valid=300



working_dir="$output/$alias"

echo "Alias: [$alias]"
echo "Private Certificate Authority: [$pca_arn]"
echo "AWS Credential Profile: [$aws_profile]"
echo "Output Directory: [$output]"
echo "Working Directory: [$working_dir]"

echo "Removing Existing Directory"
rm -r $working_dir

echo "Creating Output Directory"
mkdir -p $working_dir

echo "Changing into Output Directory"
cd $working_dir

echo "Creating Trust Store"
openssl enc -base64 -d -A <<< "/u3+7QAAAAIAAAABAAAAAgAGY2Fyb290AAABcijwXH4ABVguNTA5AAAD8zCCA+8wggLXoAMCAQICAQAwDQYJKoZIhvcNAQELBQAwgZgxCzAJBgNVBAYTAlVTMRAwDgYDVQQIEwdBcml6b25hMRMwEQYDVQQHEwpTY290dHNkYWxlMSUwIwYDVQQKExxTdGFyZmllbGQgVGVjaG5vbG9naWVzLCBJxxxxxxxxxxxxxx+UX2wmDPE1kCK4DMNEffud6QZW0CzyyRpqbn3oUYSXxmTqM6bam17jQuug0DuDPfR+uxa40l2ZvOgdFFRjKWcIfeAg5JQ4W2bHO7ZOphQazJ1FTfhy/HIrImzJ9ZVGif/L4qL8RVHHVAYBeFAlU5i38FAgMBAAGjQjBAMA8GA1Udxxxxxxxxxxxxx31GHf8ex+HO/yPbxvqNVU6pAudHEUY+9P29eykmu6lhYjcoti0q9hCGZMlwp9Kttylweeo82mMln/1otzDscPt1irdtYGeyHsi56diobwKLZw1NJldx2iD8wUpQjbEounFy4cX9mSj9g4eKfVIC6AoJoisK" >> kafka.client.truststore.jks

echo "Creating Key Store"
keytool -genkey -keystore kafka.client.keystore.jks -keyalg RSA -validity $days_valid -storepass "$passphrase" -keypass "$passphrase" -dname "CN=$alias" -alias $alias -storetype pkcs12

echo "Creating Client Certificate Signing Request"
keytool -keystore kafka.client.keystore.jks -certreq -file client-cert-sign-request -alias $alias -storepass "$passphrase" -keypass "$passphrase"

echo "Fixing Client Certificate Signing Request Headers"
sed -i.bak "1 s/^.*$/-----BEGIN CERTIFICATE REQUEST-----/" client-cert-sign-request
sed -i.bak "$ s/^.*$/-----END CERTIFICATE REQUEST-----/" client-cert-sign-request

echo "Signing Certificate w/ AWS Private Certificate Authority"
# TODO: Add support for the --no-verify-ssl being optionally passed in
if [ ! "$aws_profile" == "" ]; then
    certificate_arn="$(aws acm-pca issue-certificate --certificate-authority-arn $pca_arn --csr fileb://client-cert-sign-request --signing-algorithm "SHA256WITHRSA" --validity Value=$days_valid,Type="DAYS" --profile $aws_profile --no-verify-ssl --output text --query "CertificateArn")"
else
    certificate_arn="$(aws acm-pca issue-certificate --certificate-authority-arn $pca_arn --csr fileb://client-cert-sign-request --signing-algorithm "SHA256WITHRSA" --validity Value=$days_valid,Type="DAYS" --no-verify-ssl --output text --query "CertificateArn")"
fi

echo "Certificate ARN: [$certificate_arn]"

echo "Getting Signed Certificate from AWS Private Certificate Authority"
# TODO: Add support for the --no-verify-ssl being optionally passed in
if [ ! "$aws_profile" == "" ]; then
    aws acm-pca get-certificate --certificate-authority-arn $pca_arn --certificate-arn $certificate_arn --profile $aws_profile --no-verify-ssl --output text --query "Certificate" >> signed-certificate-from-acm
    aws acm-pca get-certificate --certificate-authority-arn $pca_arn --certificate-arn $certificate_arn --profile $aws_profile --no-verify-ssl --output text --query "CertificateChain" >> signed-certificate-from-acm
else
    aws acm-pca get-certificate --certificate-authority-arn $pca_arn --certificate-arn $certificate_arn --no-verify-ssl --output text --query "Certificate" >> signed-certificate-from-acm
    aws acm-pca get-certificate --certificate-authority-arn $pca_arn --certificate-arn $certificate_arn --no-verify-ssl --output text --query "CertificateChain" >> signed-certificate-from-acm
fi

echo "Adding Signed Certifictate to Key Store"
keytool -keystore kafka.client.keystore.jks -import -file signed-certificate-from-acm -alias $alias -storepass "$passphrase" -keypass "$passphrase" -noprompt

echo "Creating client.properties"
cat >client.properties <<'EOT'
security.protocol=SSL
ssl.truststore.location=~/msk_certs/kafka.client.truststore.jks
ssl.keystore.location=~/msk_certs/kafka.client.keystore.jks
EOT

echo "ssl.keystore.password=${passphrase}" >> client.properties
echo "ssl.key.password=${passphrase}" >> client.properties



echo "Base64 Encoding Files"
echo "...kafka.client.keystore.jks"
openssl base64 -in kafka.client.keystore.jks -out kafka.client.keystore.jks_b64 -A
echo "...kafka.client.truststore.jks"
openssl base64 -in kafka.client.truststore.jks -out kafka.client.truststore.jks_b64 -A
echo "...signed-certificate-from-acm"
openssl base64 -in signed-certificate-from-acm -out signed-certificate-from-acm_b64 -A
echo "...client.properties"
openssl base64 -in client.properties -out client.properties_b64

echo "Base64 Encoding Passphrase"
openssl enc -base64 <<< $passphrase >> passphrase_b64

echo "Zipping Contents into $output"
cd $output
zip -q -R $alias $alias/*_b64
