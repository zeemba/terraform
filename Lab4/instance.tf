resource "aws_instance" "example" {
  ami = "${lookup(var.AMIS, var.AWS_REGION)}"
  instance_type = "t2.micro"
  key_name	= "azeem"
  provisioner "local-exec" {
     command = "echo ${aws_instance.example.private_ip} >> ip.txt"
  }

  tags = {

	Name = "Lab4"
  }

}
output "ip" {
    value = "${aws_instance.example.public_ip}"
}
