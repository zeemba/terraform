source "docker" "ubuntu-noble" {
  image   = "bokf-docker.jfrog.io/ubuntu:noble"
  changes = local.dockerfile_changes
  commit  = true
}

build {
  sources = ["source.docker.ubuntu-noble"]

  provisioner "file" {
    destination = "/tmp/"
    source      = "scripts/ubuntu-docker.sh"
  }

  provisioner "shell" {
    inline = ["sh -e /tmp/ubuntu-docker.sh"]
  }

  post-processor "docker-tag" {
    repository = "bokf-lg-doc-golden.jfrog.io/ubuntu"
    tags       = local.variant_tags
  }
}
