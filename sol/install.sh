#!/bin/bash

# BOKF configuration for Ubuntu Focal
# Version 1.0.0
# Last Modified 06.01.2020

PROFILE=''
QUIET=false
SECONDS=0
            echo "running updates"
            rm -rf /var/lib/apt/lists/*
            apt update
            apt -y upgrade
            apt-get dist-upgrade -y
            apt-get clean -y            
            echo "completed updates"
            apt list --installed
            apt list --upgradeable
            sleep 2s

            # Install Terraform 1.7
            echo "Installing Terraform 1.7"
            TERRAFORM_VERSION="1.7.0"
            wget -q "https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip" -O /tmp/terraform.zip
            unzip -o /tmp/terraform.zip -d /usr/local/bin/
            rm /tmp/terraform.zip
            echo "Terraform $(terraform version) installed successfully"
            # echo "running SignalFX install"
            # Removing since it has lots of vulnerabilities
            # sh /tmp/signalfx-agent.sh
            # echo "completed SignalFX install"

            useradd -m -c "Admin User" admin
            usermod -aG sudo admin

            sed '1 s/bin/sbin/1' /etc/passwd && sed '1 s/bash/nologin/1' /etc/passwd
            sleep 2s

            #mv /etc/securetty /etc/securetty.orig
            #touch /etc/securetty
            #chmod 600 /etc/securetty

            # echo "running CIS hardening"
            # sh -e tmp/CIS_CentOS_Linux_8_Benchmark_v2.1.0.sh "Level 1 - Server"
            # echo "completed CIS hardening"            
            

duration=$SECONDS
echo "$(($duration / 60)) minutes and $(($duration % 60)) seconds elapsed."
