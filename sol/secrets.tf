module "cloud-infra-secrets" {
  source  = "app.terraform.io/bokf/secret/aws"
  version = "< 2.0.0"

  kms_key_id         = module.infra_key.kms_arn
  secret_name        = "cloud_infra_secrets"
  secret_description = "Secrets used for cloud infrastructure"
  tags = {
    environment         = var.environment
    application_id      = var.application_id
    cost_center         = var.cost_center
    asset_class         = var.asset_class
    data_classification = var.data_classification
    managed_by          = "it_cloud"
    source_code         = var.source_code
    deployed_by         = var.TFC_WORKSPACE_NAME
    requested_by        = "abc@xyz.com"
    application_role    = "app"
  }
  iam-policy-users-map = [{
    sid      = "AllowCloudEng"
    roletype = "AWS"
    role     = [data.aws_iam_role.sso_cloud_eng.arn]
    allow_actions = [
      "secretsmanager:Describe*",
      "secretsmanager:Get*",
      "secretsmanager:ListSecretVersionIds",
      "secretsmanager:RemoveRegionsFromReplication",
      "secretsmanager:ReplicateSecretToRegions",
      "secretsmanager:StopReplicationtoReplica",
      "secretsmanager:TagResource",
      "secretsmanager:UntagResource",
      "secretsmanager:ValidateResourcePolicy"
    ]
    }, {
    sid      = "AllowViewOnly"
    roletype = "AWS"
    role     = [data.aws_iam_role.sso_viewonly.arn, data.aws_iam_role.sso_sre.arn]
    allow_actions = [
      "secretsmanager:DescribeSecret",
      "secretsmanager:GetResourcePolicy",
      "secretsmanager:ListSecretVersionIds"
    ]
    }, {
    sid      = "AllowAccess"
    roletype = "AWS"
    role = [
      data.aws_iam_role.lambda_deploy_role.arn, data.aws_iam_role.admission_controller.arn, data.aws_iam_role.gitpull.arn, data.aws_iam_role.gitpull_security.arn
    ]
    allow_actions = [
      "secretsmanager:Describe*",
      "secretsmanager:Get*",
      "secretsmanager:ListSecretVersionIds"
    ]
  }]
  secret_key_value = merge({
    "regcred_ent_docker_username"    = var.REGCRED_ENT_BOKF_DOCKER_USER
    "regcred_ent_docker_api_key"     = var.REGCRED_ENT_BOKF_DOCKER
    "k8s_ac_slack_alert_channel"     = var.K8_AC_SLACK_ALERT_CHANNEL
    "k8s_ac_ala_api_key"             = var.K8_AC_ALA_API_KEY
    "k8s_ac_ala_workspace_id"        = var.K8_AC_ALA_WORKSPACE_ID
    "SN_CLOUD_INTEGRATION_CALLER_ID" = var.SN_CLOUD_INTEGRATION_CALLER_ID
    "SN_CLOUD_INTEGRATION_PASSWORD"  = var.SN_CLOUD_INTEGRATION_PASSWORD
    "SN_CLOUD_INTEGRATION_USER"      = var.SN_CLOUD_INTEGRATION_USER
    "GITLAB_K8S_RUNNER_REG_TOKEN"    = var.GITLAB_K8S_CLOUD_RUNNER_REG_TOKEN
    "cloudhealth_api_token"          = var.CLOUDHEALTH_TOKEN
    "signalfx_access_token"          = var.SIGNALFX_ACCESS_TOKEN
    "gitlab_read_token"              = var.GITLAB_READ_TOKEN
    "azure_arc_k8s_password"         = var.AZURE_ARC_K8S_SPN_PASSWORD
  }, var.environment == "tst" ? {
    "tfc_runner_sandbox_token"       = var.TFC_RUNNER_SANDBOX_TOKEN
  } : {

  })
}

module "wiz_secrets_eks" {
  source              = "app.terraform.io/bokf/secret/aws"
  secret_name         = "wiz_unified_deployment"
  secret_description  = "Secret used by wiz and kubectl-app for connecting eks for read access by wiz"
  kms_key_id          = module.infra_key.kms_arn
  tags = {
    environment         = var.environment
    application_id      = "4818"
    cost_center         = "7915"
    asset_class         = var.asset_class
    data_classification = var.data_classification
    managed_by          = "it_cloud"
    source_code         = var.source_code
    deployed_by         = var.TFC_WORKSPACE_NAME
    requested_by        = "abc@xyz.com"
    application_role    = "app"
  }
  iam-policy-users-map = [{
    sid      = "AllowCloudEng"
    roletype = "AWS"
    role     = ["arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/sso-cloud-eng"]
    allow_actions = [
      "secretsmanager:Describe*",
      "secretsmanager:Get*",
      "secretsmanager:ListSecretVersionIds",
      "secretsmanager:RemoveRegionsFromReplication",
      "secretsmanager:ReplicateSecretToRegions",
      "secretsmanager:StopReplicationtoReplica",
      "secretsmanager:TagResource",
      "secretsmanager:UntagResource",
      "secretsmanager:ValidateResourcePolicy"
    ]
  }, {
    sid      = "AllowSRE"
    roletype = "AWS"
    role     = ["arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/sso-sre"]
    allow_actions = [
      "secretsmanager:Describe*",
      "secretsmanager:Get*",
      "secretsmanager:ListSecretVersionIds",
      "secretsmanager:PutSecretValue",
      "secretsmanager:RemoveRegionsFromReplication",
      "secretsmanager:ReplicateSecretToRegions",
      "secretsmanager:StopReplicationtoReplica",
      "secretsmanager:UpdateSecret",
      "secretsmanager:UpdateSecretVersionStage"
    ]
  }, {
      sid      = "AllowViewOnly"
      roletype = "AWS"
      role     = ["arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/sso-viewonly"]
      allow_actions = [
        "secretsmanager:DescribeSecret",
        "secretsmanager:GetResourcePolicy",
        "secretsmanager:ListSecretVersionIds"
      ]
  }, {
      sid      = "AllowAccess"
      roletype = "AWS"
      role     = [data.aws_iam_role.lambda_deploy_role.arn, data.aws_iam_role.gitpull_security.arn]
      allow_actions = [
        "secretsmanager:Describe*",
        "secretsmanager:Get*",
        "secretsmanager:ListSecretVersionIds"
      ]
  }]
  secret_key_value    = { 
    "wiz_unified_deployment_id": var.wiz_unified_deployment_id,
    "wiz_unified_deployment_secret": var.wiz_unified_deployment_secret,
    "wiz_registry_username": var.wiz_registry_username,
    "wiz_registry_password": var.wiz_registry_password
  }
}
